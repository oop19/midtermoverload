/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midtermoverload;

/**
 *
 * @author a
 */
public class Finger {

    String name;
    int move;
    int movefinger;
    int namefinger;

    public Finger(String name) {
        this.name = name;
    }

    public void move(int move) {
        if (move == 1) {
            System.out.println("handful");
        } else if (move == 2) {
            System.out.println("open fingers");
        }
    }

    public void move(String name, int move) {
        if (move == 1) {
            System.out.println(name + " open");
        } else if (move == 2) {
            System.out.println(name + " clenched");
        }
    }

}
